

import Foundation
import CoreData


extension Exclusions {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Exclusions> {
        return NSFetchRequest<Exclusions>(entityName: "Exclusions")
    }

    @NSManaged public var combination: UnpredictedCombination?
    @NSManaged public var exclutionId: String?

}

extension Exclusions : Identifiable {

}
