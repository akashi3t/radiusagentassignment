

import Foundation
import CoreData


extension Facilities {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Facilities> {
        return NSFetchRequest<Facilities>(entityName: "Facilities")
    }

    @NSManaged public var createdAt: Date?
    @NSManaged public var facilityId: String?
    @NSManaged public var facilityName: String?
    @NSManaged public var updatedAt: Date?
    @NSManaged public var options: NSSet?

}

// MARK: Generated accessors for options
extension Facilities {

    @objc(addOptionsObject:)
    @NSManaged public func addToOptions(_ value: Options)

    @objc(removeOptionsObject:)
    @NSManaged public func removeFromOptions(_ value: Options)

    @objc(addOptions:)
    @NSManaged public func addToOptions(_ values: NSSet)

    @objc(removeOptions:)
    @NSManaged public func removeFromOptions(_ values: NSSet)

}

extension Facilities : Identifiable {

}
