

import Foundation
import CoreData


public class Options: NSManagedObject,RadiusAgentManagableObject {
    
    static var entityName: String = "Options"
    static func create(facId:String,id:String,icon:String,name:String)-> Options?{
        if let obj = createObject() as?  Options {
            obj.facId = facId
            obj.optionId = id
            obj.optionName = name
            obj.icon = icon
            return obj
        }
        return nil
    }
}
