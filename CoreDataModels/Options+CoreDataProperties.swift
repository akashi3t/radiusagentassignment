

import Foundation
import CoreData


extension Options {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Options> {
        return NSFetchRequest<Options>(entityName: "Options")
    }

    @NSManaged public var facId: String?
    @NSManaged public var icon: String?
    @NSManaged public var optionId: String?
    @NSManaged public var optionName: String?
    @NSManaged public var facilities: Facilities?

}

extension Options : Identifiable {

}
