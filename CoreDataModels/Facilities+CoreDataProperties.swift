//
//  Facilities+CoreDataProperties.swift
//  radiusAgentAssignment
//
//  Created by Akash Deep on 18/02/21.
//
//

import Foundation
import CoreData


extension Facilities {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Facilities> {
        return NSFetchRequest<Facilities>(entityName: "Facilities")
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?

}

extension Facilities : Identifiable {

}
