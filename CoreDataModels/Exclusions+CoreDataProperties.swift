//
//  Exclusions+CoreDataProperties.swift
//  radiusAgentAssignment
//
//  Created by Akash Deep on 18/02/21.
//
//

import Foundation
import CoreData


extension Exclusions {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Exclusions> {
        return NSFetchRequest<Exclusions>(entityName: "Exclusions")
    }

    @NSManaged public var id: Int16
    @NSManaged public var facilityId: Int16
    @NSManaged public var optionId: Int16

}

extension Exclusions : Identifiable {

}
