

import Foundation

// MARK: - Option
struct Option: Codable {
    let name, icon, id: String
}
