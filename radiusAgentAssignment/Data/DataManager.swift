

import Foundation

class DataManager {
    var  coreDataClient:CoreDataClient
    
    init(_coreDataClient:CoreDataClient) {
        coreDataClient = _coreDataClient
    }
    
    func setDataInCoreData(completion: @escaping (Bool)-> Void) {
        if let lastUpdatedDate = coreDataClient.fetchLastUpdatedTime(),
           dateDifferencsInHours(startDate:lastUpdatedDate,endDate:Date()) < 24{
            completion(true)
           return
        }
        let apiManager = ApiManager()
        apiManager.getFacilities { (dataModel, error) in
            guard error == nil else {
                completion(false)
                return
            }
            guard let data = dataModel else {
                completion(false)
                return
            }
            self.coreDataClient.insertDataIntoModels(inObjectModel: data)
            completion(true)
        }
    }
}
