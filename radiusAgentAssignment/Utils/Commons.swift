

import Foundation


func dateDifferencsInHours(startDate:Date,endDate:Date) -> Int {
    let diffComponents = Calendar.current.dateComponents([.hour], from: startDate, to: endDate)
    return diffComponents.hour ?? 0
}
