

import Foundation


enum HttpMethod {
    case post
    case get
    case put
    
    func getString()-> String {
        switch self {
        case .get:
            return "GET"
        case .post:
            return "POST"
        case .put:
            return "PUT"
        }
    }
}



enum ServerType {
    case production
    case staging
    
    func getString() -> String {
        switch self {
        case .production:
            return ""
        case .staging:
            return "https://my-json-server.typicode.com"
        }
    }
}


enum Endpoints {
    case getFacilities
    
    func getString()-> String {
        switch self {
        case .getFacilities:
            return "/iranjith4/ad-assignment/db"
        }
    }
}
