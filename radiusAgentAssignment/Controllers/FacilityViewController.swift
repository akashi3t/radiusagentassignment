

import UIKit
import iOSDropDown

class FacilityViewController: UIViewController {
    
    @IBOutlet weak var propertyTypeDropDown: DropDown!
    @IBOutlet weak var roomsDropDown: DropDown!
    @IBOutlet weak var otherDropDown: DropDown!
    
    @IBOutlet weak var propertyNameLabel: UILabel!
    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var otherNameLabel: UILabel!
    
    var facilityViewModel:FacilityViewModel = FacilityViewModel()
    var exclusions: [Exclusion] = []
    var currentSelectedOption: Options?
    
    var options1:[Options]?
    var options2:[Options]?
    var options3:[Options]?
    
    var selectedOptionIdForProperty:String?
    var selectedOptionIdForRooms:String?
    var selectedOptionIdForOthers:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDropDowns()
        setupDropDownCallbacks()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func setupDropDowns() {
        self.propertyTypeDropDown.isSearchEnable = false
        self.roomsDropDown.isSearchEnable = false
        self.otherDropDown.isSearchEnable = false
        facilityViewModel.facilities.bind(listener: {[weak self] (facilities) in
            if facilities?.count ?? 0 > 0 {
                DispatchQueue.main.async {
                    self?.propertyNameLabel.text = facilities?[0].facilityName ?? "loading.."
                    self?.roomNameLabel.text = facilities?[1].facilityName ?? "loading.."
                    self?.otherNameLabel.text = facilities?[2].facilityName ?? "loading.."
                }
            }
        })
        
        
        facilityViewModel.option1.bind { [weak self] (options) in
            self?.setOptions(inDropDown: self?.propertyTypeDropDown, options: options ?? [])
        }
        facilityViewModel.option2.bind { [weak self] (options) in
            self?.setOptions(inDropDown: self?.roomsDropDown, options: options ?? [])
        }
        facilityViewModel.option3.bind { [weak self] (options) in
            self?.setOptions(inDropDown: self?.otherDropDown, options: options ?? [])
        }
    }
    
    func setupDropDownCallbacks() {
        propertyTypeDropDown.didSelect { (selectedText, index, id) in
            self.selectedOptionIdForProperty = "\(id)"
            self.getValuesforWhichFilterNeedsToBeDone()
        }
        
        roomsDropDown.didSelect { (selectedText, index, id) in
            self.selectedOptionIdForRooms = "\(id)"
            self.getValuesforWhichFilterNeedsToBeDone()
        }
        
        otherDropDown.didSelect { (selectedText, index, id) in
            self.selectedOptionIdForOthers = "\(id)"
            self.getValuesforWhichFilterNeedsToBeDone()
        }
    }
    
    func setOptions(inDropDown:DropDown?,options:[Options]){
        var optionNameArray = [String]()
        var optionIdsArray = [Int]()
        for i in 0..<options.count {
            optionNameArray.append(options[i].optionName ?? "")
            optionIdsArray.append(Int(options[i].optionId ?? "")!)
        }
        inDropDown?.optionArray = optionNameArray
        inDropDown?.optionIds = optionIdsArray
        
    }
    
    
    func getValuesforWhichFilterNeedsToBeDone() {
        self.exclusions = []
        var exclusion:Exclusion?
        if (selectedOptionIdForProperty != nil) {
            exclusion = Exclusion(facilityID: "1", optionsID: self.selectedOptionIdForProperty!)
            self.exclusions.append(exclusion!)
        }
        if (selectedOptionIdForRooms != nil) {
            exclusion = Exclusion(facilityID: "2", optionsID: self.selectedOptionIdForRooms!)
            self.exclusions.append(exclusion!)
        }
        if (selectedOptionIdForOthers != nil) {
            exclusion = Exclusion(facilityID: "3", optionsID: self.selectedOptionIdForOthers!)
            self.exclusions.append(exclusion!)
        }
        print(exclusions)
        self.facilityViewModel.resetFacilities(exclusions: self.exclusions)
    }
   
}

