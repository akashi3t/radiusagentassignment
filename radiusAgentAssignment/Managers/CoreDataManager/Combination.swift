



import Foundation

public class UnpredictedCombination: NSObject, NSCoding {
    var combination: [Exclusion]
    
    init(exclusion: [Exclusion]) {
        self.combination = exclusion
        super.init()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        self.combination = aDecoder.decodeObject() as! [Exclusion]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(combination, forKey: "combination")
    }
}
