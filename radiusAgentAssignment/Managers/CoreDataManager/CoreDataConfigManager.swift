

import Foundation
import CoreData


class CoreDataConfigManager {
    static var sharedInstance:CoreDataConfigManager = CoreDataConfigManager()
    
    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        let container = NSPersistentCloudKitContainer(name: "radiusAgentAssignment")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
            }
        })
        return container
    }()
    
    
    
    private init() {}
    
    
    
    //MARK : get context for this persistentContainer
    func getCoreDataContext() -> NSManagedObjectContext {
        return persistentContainer.viewContext
    }

}
