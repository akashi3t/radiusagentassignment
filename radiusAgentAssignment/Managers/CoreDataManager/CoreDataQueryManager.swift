

import Foundation
import CoreData


class CoreDataQueryManager {
    let context:NSManagedObjectContext
    
    
    init(moc:NSManagedObjectContext = CoreDataConfigManager.sharedInstance.getCoreDataContext()) {
        context = moc
    }
    
    
    //MARK : Fetch facilities
    func fetchFacilities() -> [Facilities] {
        var _facilities:[Facilities]
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Facilities")
        do {
            _facilities = try context.fetch(fetchRequest) as! [Facilities]
            return _facilities
        } catch {
            print("Fetching Failed for _facilities")
            return []
        }
    }
    
    
    
    
    //MARK : Fetch Options for particular facility
    func fetchFacilities(forFacilityId:String) -> Facilities? {
        var _options:[Facilities]?
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Facilities")
        fetchRequest.predicate = NSPredicate(format: "facilityId == %@", forFacilityId)
        do {
            _options = try context.fetch(fetchRequest) as? [Facilities]
            return _options?.first
        } catch {
            print("Fetching Failed for _options for forFacilityId facility Id")
            return nil
        }
    }
    
    
    
    func fetchOptions(forFacilityId:String) -> [Options] {
        var _options = [Options]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Options")
        fetchRequest.predicate = NSPredicate(format: "facId == %@", forFacilityId)
        do {
            _options = try context.fetch(fetchRequest) as! [Options]
            return _options
        } catch {
            print("Fetching Failed for _options")
            return []
        }
        return _options
    }
    
    
    
    //MARK : Fetch Options for particular facility
    func fetchOptions(forOptionId:String) -> Options? {
        var _options:[Options]?
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Options")
        fetchRequest.predicate = NSPredicate(format: "optionId == %@", forOptionId)
        do {
            _options = try context.fetch(fetchRequest) as? [Options]
            return _options?.first
        } catch {
            return nil
        }
    }
    
    
    
    
    
    
    //MARK : Fetch Options for particular facility
    func fetchExclusions() -> [Exclusions] {
        var _exclusion = [Exclusions]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Exclusions")
        do {
            _exclusion = try context.fetch(fetchRequest) as! [Exclusions]
        } catch {
            //error
        }
        return _exclusion
        
    }
    
    //MARK: truncate xclusion table for New Data set
    func trancateExclusionTable() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Exclusions")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        trancateTable(deleteReq: deleteRequest)
    }
    
    
    
    
    //MARK: get last updated time
    func getLastUpdatedTime() -> Date? {
        var _date:Date?
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Facilities")
        fetchRequest.fetchLimit = 1
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "updatedAt", ascending: false)]
        do {
            let lastUpdatedFacility = try context.fetch(fetchRequest) as! [Facilities]
            _date = lastUpdatedFacility.first?.updatedAt
        } catch {
            print("Fetching Failed for _date for Facilities")
        }
        return _date
    }
    
    //MARK: Update options in api call
    func updateOptions(forFacility:Facility,facId:String) {
        forFacility.options.forEach { (optionItem) in
            if let optionInDB = self.fetchOptions(forOptionId: optionItem.id){
                optionInDB.setValue(optionItem.id, forKey: "optionId")
                optionInDB.setValue(optionItem.icon, forKey: "icon")
                optionInDB.setValue(optionItem.name, forKey: "optionName")
                optionInDB.setValue(facId, forKey: "facId")
                self.saveContext()
                return
            }
            _ = Options.create(facId:facId , id: optionItem.id, icon: optionItem.icon, name: optionItem.name)
            self.saveContext()
        }
    }
    
    
    //MARK: Update facilities in api call
    func updateFacility(fromDataModel:DataResponseModel) {
        fromDataModel.facilities.forEach { (item) in
            if let facility = self.fetchFacilities(forFacilityId: item.facilityID) {
                facility.setValue(item.facilityID, forKey: "facilityId")
                facility.setValue(item.name, forKey: "facilityName")
                facility.setValue(Date(), forKey: "updatedAt")
                self.updateOptions(forFacility: item, facId: item.facilityID)
                self.saveContext()
                return
            }
            _ = Facilities.create(id: item.facilityID, name: item.name)
            self.saveContext()
            self.updateOptions(forFacility: item, facId: item.facilityID)
        }
    }
    
    
    //MARK: Update exclusions for eveery api call
    func updateExclusions(fromDataModel:DataResponseModel) {
        trancateExclusionTable()
        fromDataModel.exclusions.forEach { (exclusionFromResponse) in
           _ = Exclusions.create(exclusion: exclusionFromResponse)
        }
    }
    
    
    
    
    //MARK : Insert Data for facilities
    func insertData(fromDataModel:DataResponseModel) {
        updateFacility(fromDataModel: fromDataModel)
        updateExclusions(fromDataModel: fromDataModel)
        saveContext()
    }
    
    
    //MARK : Save Context for context changes
    private func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
            } catch _{
            }
        }
    }
    
    
    //MARK: truncate table for any deleete request
    private func trancateTable(deleteReq:NSBatchDeleteRequest) {
        do {
            try context.execute(deleteReq)
        }catch _{
            
        }
    }
}
