

import Foundation
import CoreData

class CoreDataClient {
    private lazy var coreDataQueryManager:CoreDataQueryManager = CoreDataQueryManager()
        
    
    public func fetchFacilities() -> [Facilities] {
        return coreDataQueryManager.fetchFacilities()
    }
    
    public func fetchLastUpdatedTime() -> Date? {
        return coreDataQueryManager.getLastUpdatedTime()
    }
    
    public func fetchOptions(forFacilityId: String) -> [Options] {
        return coreDataQueryManager.fetchOptions(forFacilityId:forFacilityId)
    }
    
    public func fetchExclusions() -> [Exclusions] {
        return coreDataQueryManager.fetchExclusions()
    }
    
    public func insertDataIntoModels(inObjectModel:DataResponseModel) {
        coreDataQueryManager.insertData(fromDataModel: inObjectModel)
    }
}
