

import Foundation

public struct DataResponseModel: Codable {
    let facilities: [Facility]
    let exclusions: [[Exclusion]]
}
