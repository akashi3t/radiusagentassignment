


import Foundation

class ApiManager {
    func getFacilities(completionHandler:@escaping (_ data: DataResponseModel?, _ error:String?) -> Void) {
        NetworkManager.sharedInstance.callApi(endPoint: .getFacilities,
                                              method: .get,
                                              params: [:]) { (facilitiesData, apiError) in
            guard apiError == nil else {
                completionHandler(nil, apiError)
                return
            }
            completionHandler(facilitiesData, nil)
        }
    }
}

