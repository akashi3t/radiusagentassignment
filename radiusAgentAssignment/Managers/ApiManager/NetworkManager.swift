

import Foundation

class NetworkManager {
    static var sharedInstance:NetworkManager = NetworkManager()
    private let session = URLSession.shared
    private let serverType:ServerType = .staging
    
    
    private init() {
    }
    
    
    func callApi(endPoint: Endpoints,
                 method:HttpMethod,
                 params: [String: Any],
                 completionHandler:@escaping (_ data: DataResponseModel?, _ error:String?) -> Void) {
        
        let urlString:String = serverType.getString() + endPoint.getString()
        var request = URLRequest(url: URL(string: urlString)!)
        
        
        do {
            let data = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
            request.httpBody = method == .post ? data : nil
        }catch _ {
           print("data JSONSerialization error params to data")
        }
        
        
        
        let task = session.dataTask(with: request) { (data, urlResponse, apiError) in
            guard apiError == nil else {
                completionHandler(nil, "\(apiError.debugDescription)")
                return
            }
            
            guard data != nil else {
                completionHandler(nil, "\(apiError.debugDescription)")
                return
            }
            
            do {
                let facilitiesData = try JSONDecoder().decode(DataResponseModel.self, from: data!)
                completionHandler(facilitiesData, nil)
            } catch let err as NSError {
                completionHandler(nil, "\(err.debugDescription)")
            }
        }
        
        task.resume()
    }
}
