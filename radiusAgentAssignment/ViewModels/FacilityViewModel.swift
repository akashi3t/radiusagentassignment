

import Foundation

class FacilityViewModel {
    
    var facilities:Observable<[Facilities]?> = Observable(nil)
    var option1:Observable<[Options]?> = Observable(nil)
    var option2:Observable<[Options]?> = Observable(nil)
    var option3:Observable<[Options]?> = Observable(nil)
    var exclusions:[Exclusions]?
    
    var coreDataClient:CoreDataClient
    var dataManager:DataManager?
    
    init() {
        coreDataClient = CoreDataClient()
        dataManager = DataManager(_coreDataClient:coreDataClient)
        dataManager?.setDataInCoreData { (success) in
            if success {
                self.getData()
            }
        }
    }
    
    
    private func getData() {
        self.facilities.value =  coreDataClient.fetchFacilities()
        self.setOptionsForFacilities()
        self.setExclusionsForFacilities()
    }
    
    
    private func setOptionsForFacilities() {
        for number in 0..<(facilities.value?.count ?? 0){
            let facilityId = facilities.value?[number].facilityId ?? ""
            switch facilityId {
                case "1":
                    self.option1.value = coreDataClient.fetchOptions(forFacilityId: facilityId)
                    break
                case "2":
                    self.option2.value = coreDataClient.fetchOptions(forFacilityId: facilityId)
                    break
                case "3":
                    self.option3.value = coreDataClient.fetchOptions(forFacilityId: facilityId)
                    break
                default:
                    break
            }
        }
    }
    
    private func setExclusionsForFacilities() {
        exclusions = coreDataClient.fetchExclusions()
    }
    
    func getExclusion(exclusionsToFilter:[Exclusion]) -> [Exclusion] {
        var excluedOptions = [Exclusion]()
        for exclusionToFilter in exclusionsToFilter{
            let forFacilityId = exclusionToFilter.facilityID
            let forOptionId = exclusionToFilter.optionsID
            for exclusion in exclusions! {
                if  exclusion.combination?.combination.first?.facilityID == forFacilityId && exclusion.combination?.combination.first?.optionsID == forOptionId {
                    excluedOptions.append((exclusion.combination?.combination.last)!)
                }else if exclusion.combination?.combination.last?.facilityID == forFacilityId && exclusion.combination?.combination.last?.optionsID == forOptionId {
                    excluedOptions.append((exclusion.combination?.combination.first)!)
                }
            }
        }
        
        return excluedOptions
    }
    
    
    func disableOtherOptions(exclusions:[Exclusion]) {
        for index in 0..<(exclusions.count) {
            let exclusion = exclusions[index]
            let optionId = exclusion.optionsID
            let facilityId = exclusion.facilityID
            if facilityId == "1" {
                for index in 0..<(option1.value?.count ?? 0) {
                    let currOptionId = option1.value?[index].optionId
                    if currOptionId == optionId {
                        option1.value?.remove(at: index)
                        break
                    }
                }
                
            } else if facilityId == "2" {
                for index in 0..<(option2.value?.count ?? 0) {
                    let currOptionId = option2.value?[index].optionId
                    if currOptionId == optionId {
                        option2.value?.remove(at: index)
                        break
                    }
                }
            } else if facilityId == "3" {
                for index in 0..<(option3.value?.count ?? 0) {
                    let currOptionId = option3.value?[index].optionId
                    if currOptionId == optionId {
                        option3.value?.remove(at: index)
                        break
                    }
                }
            }
        }
    }
    
    func resetFacilities(exclusions:[Exclusion]) {
        markExclusions(exclusions:exclusions)
    }
    
    func resetValues() {
        self.option1.value = coreDataClient.fetchOptions(forFacilityId: "1")
        self.option2.value = coreDataClient.fetchOptions(forFacilityId: "2")
        self.option3.value = coreDataClient.fetchOptions(forFacilityId: "3")
    }
    
    func markExclusions(exclusions:[Exclusion]) {
        let exclusions = self.getExclusion(exclusionsToFilter:exclusions)
        self.resetValues()
        disableOtherOptions(exclusions: exclusions)
    }
}
