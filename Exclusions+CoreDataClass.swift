

import Foundation
import CoreData

@objc(Exclusions)
public class Exclusions: NSManagedObject,RadiusAgentManagableObject {
    static var entityName: String = "Exclusions"
    static func create(exclusion:[Exclusion]) ->  Exclusions? {
        if let obj = createObject() as? Exclusions {
            let comb = UnpredictedCombination(exclusion: exclusion)
            obj.combination = comb
            return obj
        }
        return nil
    }
}




protocol RadiusAgentManagableObject {
    static func createObject() -> RadiusAgentManagableObject?
    static var entityName:String { get }
}

extension RadiusAgentManagableObject {
    static func createObject() -> RadiusAgentManagableObject? {
        if let obj = NSEntityDescription.insertNewObject(forEntityName: Self.entityName, into:CoreDataConfigManager.sharedInstance.getCoreDataContext()) as? RadiusAgentManagableObject {
            return obj
        }
        return nil
    }
}
