

import Foundation
import CoreData


public class Facilities: NSManagedObject, RadiusAgentManagableObject {
    static var entityName: String = "Facilities"
    static func create(id:String,name:String) -> Facilities?  {
        if let obj = createObject() as?  Facilities {
            obj.facilityId = id
            obj.facilityName = name
            obj.createdAt = Date()
            obj.updatedAt = Date()
            return obj
      }
      return nil
    }
}
