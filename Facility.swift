

import Foundation

// MARK: - Facility
struct Facility: Codable {
    let facilityID, name: String
    let options: [Option]

    enum CodingKeys: String, CodingKey {
        case facilityID = "facility_id"
        case name, options
    }
}
